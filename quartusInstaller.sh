#!/bin/bash  
# Read the route of installation
echo "Where will you install Quartus?: "  
read dir_quartus 
echo "The route of installation is $dir_quartus"  
comparator="/"
if test ${dir_quartus: -1} = $comparator; then
    dir_quartus+="intelFPGA_lite/18.1"
else
    dir_quartus+="/intelFPGA_lite/18.1"
fi
echo "Quartus install in:" $dir_quartus

sudo apt-get install qemu-user-static minicom putty lib32z1 lib32ncurses6 -y
sudo add-apt-repository ppa:linuxuprising/libpng12 -y
sudo apt update
sudo apt install libpng12-0 -y
sudo apt-get upgrade qemu-user-static minicom putty lib32z1 lib32ncurses6 libpng12-0 -y

# Descargar el .tar que se vaya a instalar de la pagina oficial de quartus:
#    https://fpgasoftware.intel.com/18.1/?edition=lite&platform=linux
#    en el momento se deja en la carpeta la version quartus prime 18.1 lite, archivos combinados.
sudo apt-get install curl
echo "Acepta el acuerdo de licencia en el navegador pero no descargues el archivo."
echo "Presiona Enter para abrir el acuerdo de licencia en el navegador..."
read -p "Presiona Enter cuando hayas aceptado el acuerdo de licencia en el navegador..."
echo ""
xdg-open "https://cdrdv2.intel.com/v1/dl/getContent/665988/666036?filename=Quartus-lite-18.1.0.625-linux.tar"
read -p "Esperando aceptacion..."
echo ""
sudo wget --content-disposition https://cdrdv2.intel.com/v1/dl/getContent/665988/666036?filename=Quartus-lite-18.1.0.625-linux.tar

mv *Quartus-lite*.tar* Quartus-lite-linux.tar
mkdir Quartus-lite-linux/
tar -xvf Quartus-lite-linux.tar -C Quartus-lite-linux/

cd Quartus-lite-linux/
echo "Poner en la ruta: /opt/intelFPGA_lite/18.1/"
sudo ./setup.sh --installdir $dir_quartus
cd ..

# la instalacion del directorio ponga mejor /opt/intelFPGA_lite/18.1
# Al finalizar sale marcado para crear atajo de aplicación y lanzar, descative estas dos opciones.

sudo sh -c 'echo "" >> /etc/bash.bashrc'
sudo sh -c 'echo PATH='$PATH':'$dir_quartus'/quartus/bin:'$dir_quartus'/nios2eds/bin:'$dir_quartus'/modelsim_ase/bin >> /etc/bash.bashrc'
sudo sh -c 'echo export PATH  >> /etc/bash.bashrc'
sudo sh -c 'echo QUARTUS_ROOTDIR_OVERRIDE='$dir_quartus'/quartus  >> /etc/bash.bashrc'
sudo sh -c 'echo export QUARTUS_ROOTDIR_OVERRIDE  >> /etc/bash.bashrc'
sudo sh -c 'echo QUARTUS_64BIT=1  >> /etc/bash.bashrc'
sudo sh -c 'echo export QUARTUS_64BIT  >> /etc/bash.bashrc'

sudo sh -c 'sudo chmod 777 '$dir_quartus'/modelsim_ase/vco'
# Edit the vco script manually, or with these commands:
sudo sh -c 'sed -i 's/linux\_rh[[:digit:]]\+/linux/g' '$dir_quartus'/modelsim_ase/vco'
sudo sh -c 'sed -i 's/MTI_VCO_MODE:-""/MTI_VCO_MODE:-"32"/g' '$dir_quartus'/modelsim_ase/vco'
sudo sh -c "sed -i '/dir=\$(dirname \"\$arg0\")/a export LD_LIBRARY_PATH=\${dir}/lib32' $dir_quartus/modelsim_ase/vco"
sudo dpkg --add-architecture i386
sudo apt-get update
sudo apt-get install build-essential -y
sudo apt-get install gcc-multilib g++-multilib lib32stdc++6 lib32gcc1 expat:i386 fontconfig:i386 libfreetype6:i386 libexpat1:i386 libc6:i386 libcanberra0:i386 libice6:i386 libsm6:i386 libncurses5:i386 zlib1g:i386 libx11-6:i386 libxau6:i386 libxdmcp6:i386 libxext6:i386 libxft2:i386 libxrender1:i386 libxt6:i386 libxtst6:i386 libcanberra-gtk-module libcanberra-gtk3-module -y

sudo tar -xvf freetype-2.4.12.tar.bz2
cd freetype-2.4.12/ 
./configure --build=i686-pc-linux-gnu "CFLAGS=-m32" "CXXFLAGS=-m32" "LDFLAGS=-m32"
make -j8
sudo mkdir $dir_quartus/modelsim_ase/lib32
cd ..
sudo cp freetype-2.4.12/objs/.libs/libfreetype.so* $dir_quartus/modelsim_ase/lib32/

sudo mv $dir_quartus/quartus/linux64/libstdc++.so.6 $dir_quartus/quartus/linux64/libstdc++.so.6.quartus_distrib

sudo ln -s /usr/lib/x86_64-linux-gnu/libstdc++.so.6 $dir_quartus/quartus/linux64/libstdc++.so.6

sudo touch /usr/share/applications/quartus.desktop

sudo sh -c 'echo [Desktop Entry] >> /usr/share/applications/quartus.desktop'
sudo sh -c 'echo Version=18.1 >> /usr/share/applications/quartus.desktop'
sudo sh -c 'echo Name=Quartus Prime 18.1 >> /usr/share/applications/quartus.desktop'
sudo sh -c 'echo Comment=Lite Edition >> /usr/share/applications/quartus.desktop'
sudo sh -c 'echo Exec='$dir_quartus'/quartus/bin/quartus --64bit >> /usr/share/applications/quartus.desktop'
sudo sh -c 'echo Icon='$dir_quartus'/quartus/adm/quartusii.png >> /usr/share/applications/quartus.desktop'
sudo sh -c 'echo Terminal=false >> /usr/share/applications/quartus.desktop'
sudo sh -c 'echo Type=Application >> /usr/share/applications/quartus.desktop'
sudo sh -c 'echo Categories=Development >> /usr/share/applications/quartus.desktop'
sudo sh -c 'echo StartupWMClass=Quartus >> /usr/share/applications/quartus.desktop'
sudo sh -c 'echo StartupNotify=true >> /usr/share/applications/quartus.desktop'

echo "Acepta el acuerdo de licencia en el navegador pero no descargues el archivo."
echo "Presiona Enter para abrir el acuerdo de licencia en el navegador..."
read -p "Presiona Enter cuando hayas aceptado el acuerdo de licencia en el navegador..."
echo ""
xdg-open "https://cdrdv2.intel.com/v1/dl/getContent/665456/665488?filename=SoCEDSSetup-18.1.0.625-linux.run"
read -p "Esperando aceptacion..."
echo ""
sudo wget --content-disposition "https://cdrdv2.intel.com/v1/dl/getContent/665456/665488?filename=SoCEDSSetup-18.1.0.625-linux.run"
mv *SoCEDSSetup*.run SoCEDSSetup-linux.run
sudo chmod 777 SoCEDSSetup-linux.run

sudo ./SoCEDSSetup-linux.run --installdir $dir_quartus

sudo sh -c 'echo "# For Altera USB-Blaster permissions." >> /etc/udev/rules.d/51-usbblaster.rules'
sudo sh -c 'echo SUBSYSTEM=="usb", >> /etc/udev/rules.d/51-usbblaster.rules'
sudo sh -c 'echo ENV{​​​​​​DEVTYPE}​​​​​​​​​​​​​=="usb_device", >> /etc/udev/rules.d/51-usbblaster.rules'
sudo sh -c 'echo ATTR{​​​​​​​​​​​​​​​​​​​​idVendor}​​​​​​​​​​​​​​​​​​​​​​​​​​​=="09fb", >> /etc/udev/rule's.d/51-usbblaster.rules
sudo sh -c 'echo ATTR{​​​​​​​​​​​​​​​​​​​​​​​​​​​idProduct}​​​​​​​​​​​​​​​​​​​​​​​​​​​=="6010", >> /etc/udev/rules.d/51-usbblaster.rules'
sudo sh -c 'echo MODE="0666", >> /etc/udev/rules.d/51-usbblaster.rules'
sudo sh -c 'echo NAME="bus/usb/$env{​​​​​​​​​​​​​​​​​​​​​​​​​​​BUSNUM}​​​​​​​​​​​​​​​​​​​​​​​​​​​/$env{​​​​​​​​​​​​​​​​​​​​​​​​​​​DEVNUM}​​​​​​​​​​​​​​​​​​​​​​​​​​​", >> /etc/udev/rules.d/51-usbblaster.rules'
sudo sh -c 'echo RUN+="/bin/chmod 0666 %c" >> /etc/udev/rules.d/51-usbblaster.rules'

sudo sh -c 'echo "# For Altera USB-Blaster permissions." >> /etc/udev/rules.d/51-usbblaster.rules'
sudo sh -c 'echo SUBSYSTEM=="usb", >> /etc/udev/rules.d/51-usbblaster.rules'
sudo sh -c 'echo ENV{​​​​​​DEVTYPE}​​​​​​​​​​​​​=="usb_device", >> /etc/udev/rules.d/51-usbblaster.rules'
sudo sh -c 'echo ATTR{​​​​​​​​​​​​​​​​​​​​idVendor}​​​​​​​​​​​​​​​​​​​​​​​​​​​=="09fb", >> /etc/udev/rules.d/51-usbblaster.rules'
sudo sh -c 'echo ATTR{​​​​​​​​​​​​​​​​​​​​​​​​​​​idProduct}​​​​​​​​​​​​​​​​​​​​​​​​​​​=="6810", >> /etc/udev/rules.d/51-usbblaster.rules'
sudo sh -c 'echo MODE="0666", >> /etc/udev/rules.d/51-usbblaster.rules'
sudo sh -c 'echo NAME="bus/usb/$env{​​​​​​​​​​​​​​​​​​​​​​​​​​​BUSNUM}​​​​​​​​​​​​​​​​​​​​​​​​​​​/$env{​​​​​​​​​​​​​​​​​​​​​​​​​​​DEVNUM}​​​​​​​​​​​​​​​​​​​​​​​​​​​", >> /etc/udev/rules.d/51-usbblaster.rules'
sudo sh -c 'echo RUN+="/bin/chmod 0666 %c" >> /etc/udev/rules.d/51-usbblaster.rules'

# Finalmente conetca la tarjeta, pon jtagconfig si no la regsitra haz lo siguiente.

# Se utiliza el siguiente comando  dmesg | tail donde saldrá la tarjeta para tomar este dato.

# de ahí miras esta linea "[21800.124391] usb 1-1: New USB device found, idVendor=09fb, idProduct=6810, bcdDevice= 0.01"

# tomas idVendor e idProduct

# Si no sirve las reglase revisa idVendor e idProduct

sudo rm -rf Quartus-lite-linux/
sudo rm Quartus-lite-linux.tar
sudo rm SoCEDSSetup-linux.run
# Referencias:
#     1. https://www.youtube.com/watch?v=WHI5Xz7jycA
#     2. https://askubuntu.com/questions/342202/failed-to-load-module-canberra-gtk-module-but-already-installed
#     3. http://www.stevesmuddlings.org/2015/09/test-post-1.html